using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using Random = UnityEngine.Random;

public class MonsterSpawner : MonoBehaviour
{
    public List<Transform> spawnPositions = new List<Transform>();

    public GameObject monsterPrefab;

    public float minSpawnTime = 2f;
    public float maxSpawnTime = 5f;

    private float spawnTime;
    private int randomPos;
    private int curLevel;

    private float curMinSpawnTime;
    private float curMaxSpawnTime;

    private void Start()
    {
        spawnTime = Random.Range(minSpawnTime, maxSpawnTime);
        randomPos = Random.Range(0, spawnPositions.Count);
        curLevel = GameManager.Instance.GetLevel();

        curMinSpawnTime = minSpawnTime;
        curMaxSpawnTime = maxSpawnTime;
    }

    private void Update()
    {
        SpawnAfterCertainTime();
        SetSpawnTime();
    }

    private void SetSpawnTime()
    {
        if (curLevel != GameManager.Instance.GetLevel())
        {
            curLevel = GameManager.Instance.GetLevel();

            minSpawnTime = Mathf.Clamp(minSpawnTime - 0.15f, 0.5f, curMinSpawnTime);
            maxSpawnTime = Mathf.Clamp(maxSpawnTime - 0.3f, 2f, curMaxSpawnTime);
        }
    }

    private void SpawnAfterCertainTime()
    {
        if (!GameManager.Instance.isFreezeActive)
        {
            spawnTime -= Time.deltaTime;
        }

        if (spawnTime < 0f)
        {
            spawnTime = Random.Range(minSpawnTime, maxSpawnTime);

            Monster monster = Instantiate(monsterPrefab, spawnPositions[randomPos].position, Quaternion.identity, gameObject.transform).GetComponent<Monster>();
            randomPos = Random.Range(0, spawnPositions.Count);
            
            monster.SetHealth(GetHealthAccordingToLevel());
            monster.GetComponent<NavMeshAgent>().speed = GetMoveSpeedAccordingToLevel();

            GameManager.Instance.monsters.Add(monster);
            
            UpdateUI();
        }

        if (GameManager.Instance.monsters.Count >= 10)
        {
            GameManager.Instance.Lose();
            enabled = false;
        }
    }

    private int GetHealthAccordingToLevel()
    {
        return 5 + GameManager.Instance.GetLevel();
    }

    public float GetMoveSpeedAccordingToLevel()
    {
        return 2 + GameManager.Instance.GetLevel() * 0.25f;
    }

    private void UpdateUI()
    {
        GameUI.Instance.SetScoreText(GameManager.Instance.GetScore());
        GameUI.Instance.SetMonsterText(GameManager.Instance.monsters.Count);
    }
}
