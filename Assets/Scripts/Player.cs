using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player
{
    public string nickname;
    public int score;

    public Player(string nickname, int score)
    {
        this.nickname = nickname;
        this.score = score;
    }
}
