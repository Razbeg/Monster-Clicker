using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public enum BoosterType
{
    Kill,
    Freeze
}

public class Booster : MonoBehaviour
{
    private BoosterType type;
    private Renderer rd;

    private void Awake()
    {
        rd = GetComponentInChildren<MeshRenderer>();
    }

    private void Start()
    {
        type = (BoosterType) Random.Range(0, 2);
        
        switch (type)
        {
            case BoosterType.Kill:
                rd.material.color = Color.red;
                break;
            case BoosterType.Freeze:
                rd.material.color = Color.blue;
                break;
        }
    }

    private void Update()
    {
        Rotate();
    }

    public BoosterType GetBoosterType()
    {
        return type;
    }

    private void Rotate()
    {
        transform.Rotate(Vector3.right);
    }
}
