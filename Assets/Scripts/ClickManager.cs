using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClickManager : MonoBehaviour
{
    public static ClickManager Instance { get; private set; }
    
    public int hitDamage = 1;
    public LayerMask layer;
    public GameObject hitVFXPrefab;

    private Camera cam;
    private Coroutine freeze;

    private void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(gameObject);
        }
        else
        {
            Instance = this;
        }
    }

    private void Start()
    {
        cam = Camera.main;
    }

    private void Update()
    {
        if (cam == null)
        {
            cam = Camera.main;
        }
        
        if (Input.touches.Length > 0 && Input.touches[0].phase == TouchPhase.Began)
        {
            Click();
        }
    }

    private void Click()
    {
        Ray ray = cam.ScreenPointToRay(Input.touches[0].position);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit, layer))
        {
            if (hit.collider.CompareTag("Monster"))
            {
                hit.collider.GetComponent<Monster>().Clicked(hitDamage);
                
                HitVFX(hit.collider.transform);
            }

            if (hit.collider.CompareTag("Booster"))
            {
                switch (hit.collider.GetComponent<Booster>().GetBoosterType())
                {
                    case BoosterType.Freeze:
                        if (freeze != null)
                        {
                            StopCoroutine(freeze);
                        }

                        freeze = StartCoroutine(GameManager.Instance.FreezeBooster());
                        
                        Destroy(hit.collider.gameObject);
                        break;
                    case BoosterType.Kill:
                        GameManager.Instance.KillAllBooster();
                        
                        Destroy(hit.collider.gameObject);
                        break;
                }
            }
        }
    }

    private void HitVFX(Transform location)
    {
        GameObject vfx = Instantiate(hitVFXPrefab, location.position + new Vector3(0, 2f, 0), Quaternion.identity);
        
        Destroy(vfx, 1f);
    }
}
