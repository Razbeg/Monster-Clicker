using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance { get; private set; }
    
    public List<Monster> monsters = new List<Monster>();
    public List<Player> players = new List<Player>();

    public bool isFreezeActive;
    public float freezeDuration;

    [SerializeField] private int levelIncreaseInSeconds = 10;
    [SerializeField] private int gameLevel = 1;
    [SerializeField] private int score = 0;

    private Coroutine levelIncrease;

    private void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(gameObject);
        }
        else
        {
            Instance = this;
        }
        
        DontDestroyOnLoad(this);
    }

    public void KillAllBooster()
    {
        Monster[] monsterArray = FindObjectsOfType<Monster>();

        foreach (Monster monster in monsterArray)
        {
            StartCoroutine(monster.Die());
        }
    }

    public IEnumerator FreezeBooster()
    {
        isFreezeActive = true;
        yield return new WaitForSeconds(freezeDuration);
        isFreezeActive = false;
        
    }

    private IEnumerator LevelIncrease()
    {
        while (monsters.Count < 10)
        {
            yield return new WaitForSeconds(levelIncreaseInSeconds);
            gameLevel++;
        }
    }

    public void ResetGameManager()
    {
        levelIncreaseInSeconds = 10;
        gameLevel = 1;
        score = 0;
        Time.timeScale = 1;
        isFreezeActive = false;
        levelIncrease = StartCoroutine(LevelIncrease());
    }

    public void BackToMainMenu()
    {
        players[players.Count - 1].score = score;

        StopCoroutine(levelIncrease);
    }

    public void IncreaseScore(int amount = 1)
    {
        score += amount;
    }
        
    public int GetLevel()
    {
        return gameLevel;
    }

    public int GetScore()
    {
        return score;
    }

    public void Lose()
    {
        Debug.Log("You Lost!");
        GameUI.Instance.EndGame();
    }
}
