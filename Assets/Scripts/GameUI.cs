using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameUI : MonoBehaviour
{
    public static GameUI Instance { get; private set; }

    public GameObject endGameUI;
    public GameObject inGameUI;
    public GameObject inGameMenuUI;
    public TextMeshProUGUI scoreText;
    public TextMeshProUGUI monsterText;
    
    private void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(gameObject);
        }
        else
        {
            Instance = this;
        }
    }

    public void SetScoreText(int amount)
    {
        scoreText.text = "Score: " + amount;
    }

    public void SetMonsterText(int amount)
    {
        monsterText.text = "Monsters: " + amount;
    }

    public void EndGame()
    {
        Time.timeScale = 0;
        endGameUI.SetActive(true);
    }

    public void OnEscButton()
    {
        Time.timeScale = 0;
        inGameMenuUI.SetActive(true);
    }
    
    public void OnContinueButton()
    {
        Time.timeScale = 1;
        inGameMenuUI.SetActive(false);
    }

    public void OnMainMenuButton()
    {
        GameManager.Instance.BackToMainMenu();
        SceneManager.LoadScene("Main Menu");
    }

    public void OnPlayAgainButton()
    {
        GameManager.Instance.ResetGameManager();
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
}
