using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class PlayerRank : MonoBehaviour
{
    public TextMeshProUGUI playerNicknameText;
    public TextMeshProUGUI playerScoreText;
}
