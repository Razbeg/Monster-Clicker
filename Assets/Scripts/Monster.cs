using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using Random = UnityEngine.Random;

public class Monster : MonoBehaviour
{
    public List<AudioClip> clips = new List<AudioClip>();
    
    public float wanderRadius = 5f;
    public float wanderTime = 3f;

    public GameObject boosterPrefab;
    
    [SerializeField]
    private int health = 5;
    
    private NavMeshAgent agent;
    private AudioSource source;
    private Coroutine wander;
    private Animator anim;
    private bool isDead;
    private int randClipIndex;

    private void Awake()
    {
        agent = GetComponent<NavMeshAgent>();
        anim = GetComponent<Animator>();
        source = GetComponent<AudioSource>();
    }

    private void Start()
    {
        PlayAudioClip();
        isDead = false;
        wander = StartCoroutine(Wander());
    }

    private void LateUpdate()
    {
        AnimationState();
    }

    private void AnimationState()
    {
        if (agent.remainingDistance < 0.1f || GameManager.Instance.isFreezeActive)
        {
            agent.isStopped = true;
            anim.SetBool("isWalking", false);
        }
        else
        {
            agent.isStopped = false;
            anim.SetBool("isWalking", true);
        }
    }

    private IEnumerator Wander()
    {
        while (true)
        {
            if (!GameManager.Instance.isFreezeActive)
            {
                agent.SetDestination(GetWanderLocation());
            }
            
            yield return new WaitForSeconds(wanderTime);
            
            PlayAudioClip();
        }
    }

    private Vector3 GetWanderLocation()
    {
        Vector3 pos = transform.position;
        Vector2 randomPos = Random.insideUnitCircle.normalized * wanderRadius;
        pos.x += randomPos.x;
        pos.z += randomPos.y;

        return pos;
    }

    public void Clicked(int amount)
    {
        health -= amount;

        if (health <= 0 && !isDead)
        {
            StartCoroutine(Die());
        }
    }

    public void SetHealth(int amount)
    {
        health = amount;
    }

    public IEnumerator Die()
    {
        GameManager.Instance.monsters.Remove(this);
        GameManager.Instance.IncreaseScore(GameManager.Instance.GetLevel());
        
        UpdateUI();

        isDead = true;
        
        StopCoroutine(wander);
        
        agent.isStopped = true;
        anim.SetTrigger("dead");

        GetComponent<BoxCollider>().enabled = false;

        yield return new WaitForSeconds(2f);

        GetBoosterChance(); 
        
        
        
        Destroy(gameObject);
    }

    private void GetBoosterChance()
    {
        int rand = Random.Range(0, 101);

        if (rand <= 25 || rand > 100 - GameManager.Instance.GetLevel() * 0.25f)
        {
            Instantiate(boosterPrefab, new Vector3(transform.position.x, transform.position.y + 2f, transform.position.z), Quaternion.identity);
        }
    }

    private void PlayAudioClip()
    {
        randClipIndex = Random.Range(0, clips.Count);
        source.clip = clips[randClipIndex];
        source.Play();
    }
    
    private void UpdateUI()
    {
        GameUI.Instance.SetScoreText(GameManager.Instance.GetScore());
        GameUI.Instance.SetMonsterText(GameManager.Instance.monsters.Count);
    }
}
