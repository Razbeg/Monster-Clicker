using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public float speed;

    public float minXDist;
    public float maxXDist;
    public float minZDist;
    public float maxZDist;

    private void Update()
    {
        if (Input.touches.Length > 0 && Input.touches[0].phase == TouchPhase.Moved)
        {
            Vector2 touchPos = Input.touches[0].deltaPosition;

            Vector3 dir = -transform.forward * touchPos.y + -transform.right * touchPos.x;

            transform.position += dir * (speed * Time.deltaTime);

            transform.position = new Vector3
            (
                Mathf.Clamp(transform.position.x, minXDist, maxXDist),
                transform.position.y,
                Mathf.Clamp(transform.position.z, minZDist, maxZDist)
            );
        }
    }
}
