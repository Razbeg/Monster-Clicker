using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenuUI : MonoBehaviour
{
    public static MainMenuUI Instance { get; private set; }

    public GameObject rankPrefab;
    public Transform rankList;
    
    public GameObject mainMenuUI;
    public GameObject ranksUI;
    public GameObject creditsUI;
    public TMP_InputField playerNickname;
    public Button newGameButton;
    
    private void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(gameObject);
        }
        else
        {
            Instance = this;
        }
    }

    private void Start()
    {
        for (int i = 0; i < GameManager.Instance.players.Count; i++)
        {
            PlayerRank playerRank = Instantiate(rankPrefab, rankList).GetComponent<PlayerRank>();
            playerRank.playerNicknameText.text = GameManager.Instance.players[i].nickname;
            playerRank.playerScoreText.text = GameManager.Instance.players[i].score.ToString();
        }
    }

    private void Update()
    {
        if (playerNickname.text == "")
        {
            newGameButton.interactable = false;
        }
        else
        {
            newGameButton.interactable = true;
        }
    }

    public void OnNewGameButton()
    {
        GameManager.Instance.players.Add(new Player(playerNickname.text, 0));
        GameManager.Instance.ResetGameManager();
        SceneManager.LoadScene("Game");
    }

    public void OnRanksButton()
    {
        mainMenuUI.SetActive(false);
        ranksUI.SetActive(true);
    }

    public void OnCreditsUI()
    {
        mainMenuUI.SetActive(false);
        creditsUI.SetActive(true);
    }

    public void OnBackButton()
    {
        mainMenuUI.SetActive(true);
        ranksUI.SetActive(false);
        creditsUI.SetActive(false);
    }

    public void OnQuitButton()
    {
        Application.Quit();
    }
}
